# Craft Weather Plugin

This plugin is designed to allow you to quickly and easily connect to Yahoo's Weather API to get the current weather and forecast for an area, based on zip code or woeid

## Example Usage
	*Create an instance of weather*
	{% set weather = craft.weather.woeid('4118').unit('c').get %}

	*Display Current Conditions*
	The temperature is {{ weather.temp }}, the condition is {{ weather.condition }} at {{ weather.datetime }}


	*Show the 5 day forecast*
	{% for day in forecast %}
		{{ day.day }},  {{ day.date }} <br />
		Low of {{ day.low }} <br />
		High of {{ day.high }} <br />
		Condtions: {{ day.text }} <br />
	{% endfor %}


## Parameters

The tag accepts the following paramaters:

* woeid - Set the location of the weather by woeid
* zip - Set the location of the weather by zip code
* unit - Set the unit (F or C) of the derees