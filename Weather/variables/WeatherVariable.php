<?php
namespace Craft;

class WeatherVariable
{
	private $url = 'http://query.yahooapis.com/v1/public/yql?format=json&q=';
    private $location, $weather, $unit;

    public function woeid($woeid = '') {
        $this->woeid = $woeid;
        return $this;
    }

    public function zip($zip = '') {
        $this->zip = $zip;
        return $this;
    }

    public function unit($unit = 'f') {
        $this->unit = $unit;
        return $this;
    }

    public function get() {
        if (!empty($this->zip)) {
            $query = 'select%20item%20from%20weather.forecast%20where%20location='.$this->location;
        }
        if (!empty($this->woeid)) {
            $query = 'select%20item%20from%20weather.forecast%20where%20woeid='.$this->woeid;
        }
        if (strtolower($this->unit) == 'c') {
            $query .= '%20AND%20u="c"';
        }
        $this->weather = file_get_contents($this->url.$query);
        $this->weather = json_decode($this->weather);
        return $this;
    }

    public function temp() {
        $temperature = $this->weather->query->results->channel->item->condition->temp;
        return $temperature;
    }

    public function condition() {
        return $this->weather->query->results->channel->item->condition->text;
    }

    public function datetime() {
        return $this->weather->query->results->channel->item->condition->date;
    }

    public function forecast() {
        return $this->weather->query->results->channel->item->forecast;
    }

    public function day($days = 0) {

        return $this->weather->query->results->channel->item->forecast[$days];
    }

}
