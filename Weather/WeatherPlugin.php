<?php
namespace Craft;

class WeatherPlugin extends BasePlugin
{
    function getName()
    {
        return Craft::t('Weather');
    }

    function getVersion()
    {
        return '0.1';
    }

    function getDeveloper()
    {
        return 'Masuga Design';
    }

    function getDeveloperUrl()
    {
        return 'http://masugadesign.com';
    }

    function hasCpSection()
    {
        return true;
    }
}